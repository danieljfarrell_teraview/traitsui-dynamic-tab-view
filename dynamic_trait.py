'Test case, showing solution to dynamically constructed view problem.'

from traits.api   import HasTraits, Str, Float, Instance
from traitsui.api import View, Item, ModelView, InstanceEditor


class SimpleModel(HasTraits):

    info = Str('example')

    
class SimpleView(ModelView):

    model = Instance(SimpleModel)
    
    def default_traits_view(self):
        view = View(
            Item('model.info'),
            Item('model.test'),
            title='SimpleView')
        return view

if(__name__ == '__main__'):
    # Test simple view
    model = SimpleModel(info='Blah')
    model.add_trait('test', Float(0.0))
    view = SimpleView(model=model)
    view.configure_traits()
